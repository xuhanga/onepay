<?php

/**
 *  +----------------------------------------------------------------------
 *  | 草帽支付系统 [ WE CAN DO IT JUST THINK ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2018 http://www.iredcap.cn All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed ( https://www.apache.org/licenses/LICENSE-2.0 )
 *  +----------------------------------------------------------------------
 *  | Author: Brian Waring <BrianWaring98@gmail.com>
 *  +----------------------------------------------------------------------
 */

namespace app\common\logic;


use app\common\library\enum\CodeEnum;
use app\common\library\exception\OrderException;
use think\Db;
use think\Log;

class OrdersResult extends BaseLogic
{


    /**
     * 记录支付订单接口回调给商户的数据
     *
     *
     * @param $orderData
     * @return mixed
     * @throws OrderException
     */
    public function createOrderResult($orderData){
        //TODO 事务处理
        Db::startTrans();
        try{
            $order = new OrdersResult();
            $order->out_trade_no = $orderData['charge']['out_trade_no'];//商户单号
            $order->result     = json_encode(!empty($orderData['charge']) ?$orderData['charge']:[]);
            $order->pay_id = $orderData['pay_id'];
            $order->save();

            Db::commit();

            return $order;

        }catch (\Exception $e){
            //记录日志
            Log::error("Create OrderResult Error:[{$e->getMessage()}]");

            Db::rollback();
            //抛出错误异常
            throw new OrderException([
                'msg'   =>  "Create OrderResult Error, Please Try Again Later.",
                'errCode'=> '200001'
            ]);
        }
    }


    public function getOutTradeOrderResult($where = [], $field = true){

        return $this->modelOrdersResult->getInfo($where, $field);

    }

}